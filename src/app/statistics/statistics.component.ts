import { Component, OnInit, ViewChild } from '@angular/core';
import { CommentsService } from '../services/comments.service';
import { take } from 'rxjs/operators';
import { MatPaginator, MatTableDataSource, MatSort } from '@angular/material';

@Component({
  selector: 'app-statistics',
  templateUrl: './statistics.component.html',
  styleUrls: ['./statistics.component.scss']
})
export class StatisticsComponent implements OnInit {
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;
  @ViewChild(MatSort) sort: MatSort;
  dataSource = new MatTableDataSource();
  constructor(private commentsService: CommentsService) { }
  
  ngOnInit(): void {
    this.commentsService.statistics().pipe(take(1))
      .subscribe(data => {
        this.dataSource = new MatTableDataSource(data);
        this.dataSource.paginator = this.paginator;
        this.dataSource.sort = this.sort;
      });
  }



}
